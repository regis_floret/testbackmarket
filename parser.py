#!/usr/bin/env python3
import re
import sys

import collections

REGEX_ELEMENTS = r'(A[cglmrstu]|B[aehikr]?|C[adeflmnorsu]?|D[bsy]|E[rsu]|F[elmr]?|G[ade]|H[efgos]?|I[nr]?|Kr?|' \
                 r'L[airuv]|M[dgnot]|N[abdeiop]?|Os?|P[abdmortu]?|R[abefghnu]|S[bcegimnr]?|T[abcehilm]|U(u[opst])?|' \
                 r'V|W|Xe|Yb?|Z[nr])'
REGEX_FIND_CURLY = r'\{(.*?)\}(\d*)'
REGEX_FIND_SQUARE = r'\[(.*?)\](\d*)'
REGEX_FIND_ROUND = r'\((.*?)\)(\d*)'
REGEX_FIND_ELEMENTS = REGEX_ELEMENTS + r'(\d+)'


def remove_empty(to_clean):
    """
    Remove empty element of a list

    :param to_clean: THe list to clean
    :type to_clean: list
    :return: THe list cleaned
    :rtype: list
    """
    return [el for el in to_clean if el]


def normalize_string(regex, expr, container=''):
    """
    Normalize a string by find the 'raw' and multiply it. 
        
    :param regex: The regex string
    :type regex: str
    :param expr: The expression to evaluate 
    :type expr: str
    :param container: The container
    :type container: str
    :return: 
    :rtype: str
    """
    start, end = (container[0], container[1]) if container else ('', '')
    regex = re.compile(regex)
    regex_all = regex.findall(expr)
    if len(regex_all):

        for r in regex_all:
            empty_removed = remove_empty(r)

            if len(empty_removed) == 1:
                empty_removed = (empty_removed[0], '1')

            token, multi = empty_removed
            multi_str = multi if multi != '1' else ''

            rebuild = '{start}{letter}{end}{multi}'.format(letter=token, multi=multi_str, start=start, end=end)

            raw_str = token * int(multi) if multi else token
            expr = expr.replace(rebuild, raw_str)
    return expr


def parse_molecule(expr):
    """
    :param expr: The molecule expression
    :type expr: str
    :return: The atoms count
    :rtype: dict
    """
    expr = normalize_string(REGEX_FIND_CURLY, expr, '{}')
    expr = normalize_string(REGEX_FIND_SQUARE, expr, '[]')
    expr = normalize_string(REGEX_FIND_ROUND, expr, '()')
    expr = normalize_string(REGEX_FIND_ELEMENTS, expr)

    result = collections.defaultdict(int)
    for r in remove_empty(re.split(REGEX_ELEMENTS, expr)):
        if r:
            result[r] += 1

    return dict(result)


def main():
    """ Start point. Avoid scope pollution """
    if len(sys.argv) == 1:
        sys.exit("A molecule expression is required")

    if len(sys.argv) > 2:
        sys.exit('Only one molecule expression is required')

    print(parse_molecule(sys.argv[1]))


if __name__ == '__main__':
    main()
