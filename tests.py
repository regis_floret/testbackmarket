"""
Unit test for Molecule parser
"""
import unittest

from parser import parse_molecule, normalize_string, REGEX_FIND_ROUND, REGEX_FIND_ELEMENTS, REGEX_FIND_SQUARE, REGEX_FIND_CURLY


class TestParser(unittest.TestCase):
    def test_parser_with_water(self):
        water = 'H2O'
        expected = {'H': 2, 'O': 1}
        result = parse_molecule(water)
        self.assertEqual(result, expected)

    def test_parser_with_magnesium_hydroxide(self):
        magnesium_hydroxide = 'Mg(OH)2'
        expected = {'Mg': 1, 'O': 2, 'H': 2}
        result = parse_molecule(magnesium_hydroxide)
        self.assertEqual(result, expected)

    def test_parser_with_fremy_salt(self):
        fremy_salt = 'K4[ON(SO3)2]2'
        expected = {'K': 4, 'O': 14, 'N': 2, 'S': 4}
        result = parse_molecule(fremy_salt)
        self.assertEqual(result, expected)

    def test_parser_with_iron_nitrate(self):
        iron_nitrate = 'Fe(NO3)2'
        expected = {'Fe': 1, 'O': 6, 'N': 2}
        result = parse_molecule(iron_nitrate)
        self.assertEqual(result, expected)

    def test_normalize_string_without_brackets(self):
        expression = 'H2O2Na'
        expected = 'HHOONa'
        result = normalize_string(REGEX_FIND_ELEMENTS, expression)

        self.assertEqual(result, expected)

    def test_normalize_string_with_round_brackets(self):
        expression = '(NaO)2(HO)'
        expected = 'NaONaOHO'
        result = normalize_string(REGEX_FIND_ROUND, expression, '()')
        self.assertEqual(result, expected)

    def test_normalize_string_with_square_brackets(self):
        expression = '[NaO]2[H]'
        expected = 'NaONaOH'
        result = normalize_string(REGEX_FIND_SQUARE, expression, '[]')
        self.assertEqual(result, expected)

    def test_normalize_string_with_curly_brackets(self):
        expression = '{NaO}2{Ti}'
        expected = 'NaONaOTi'
        result = normalize_string(REGEX_FIND_CURLY, expression, '{}')
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
